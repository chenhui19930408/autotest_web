import os
import time


class DirPath:
    # 获取当前时间
    now_time = time.strftime('%Y-%m-%d_%H_%M_%S')
    # 获取根目录
    rootPath = os.path.dirname(__file__)
    # 配置文件目录
    config_path = rootPath + '/config'
    # allure的启动目录
    allure = rootPath + '/common/allure/bin/allure.bat'

    # 测试报告根目录
    reports_path = rootPath + '/reports'
    # 内存测试报告目录
    memory_test_report_path = reports_path + '/memory_test_report'
    # 回归测试报告目录
    regression_test_report_path = reports_path + '/regression_test_report'
    # 回归测试报告的资源目录
    report_source = regression_test_report_path + '/' + now_time + '/result'
    # 回归测试报告的生成报告目录
    report_html = regression_test_report_path + '/' + now_time + '/report'

    # testcase的路径
    testcase_path = rootPath + '/testcase'
    # 内存测试用例路径
    memory_test_case_path = testcase_path + '/memory_test'
    # 回归测试用例路径
    regression_test_case_path = testcase_path + '/regression_test'

    # 存放日志的目录
    log_path = rootPath + '/logs'


if __name__ == "__main__":
    print(DirPath.now_time)
    print(DirPath.rootPath)
    print(DirPath.allure)

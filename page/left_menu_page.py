# -*- encoding: utf-8 -*-
"""
@File    : left_menu_page.py
@Time    : 2021/6/3
@Author  : 陈晖
"""
from time import sleep
from common.webdriver import WebDriver


class LeftMenuPage:

    user_schedule_xpath = '//*[text()="人员排班"]'
    team_group_manage_xpath = '//*[text()="班组管理"]'
    user_equipment_xpath = '//*[text()="机台分配"]'
    schedule_manage_xpath = '//*[text()="排班管理"]'

    base_data_xpath = '//*[text()="基础数据"]'
    dictionary_xpath = '//*[text()="数据字典"]'
    product_user_xpath = '//*[text()="生产人员"]'
    equipment_manage_xpath = '//*[text()="生产设备"]'
    product_kind_manage_xpath = '//*[text()="品种管理"]'
    color_manage_xpath = '//*[text()="颜色管理"]'

    def __init__(self):
        self.driver = WebDriver.get_webdriver()

    def get_user_schedule(self):
        return self.driver.find_element_by_xpath(self.user_schedule_xpath)

    def get_team_group_manage(self):
        return self.driver.find_element_by_xpath(self.team_group_manage_xpath)

    def get_user_equipment(self):
        return self.driver.find_element_by_xpath(self.user_equipment_xpath)

    def get_schedule_manage(self):
        return self.driver.find_element_by_xpath(self.schedule_manage_xpath)

    def get_base_data(self):
        return self.driver.find_element_by_xpath(self.base_data_xpath)

    def get_dictionary(self):
        return self.driver.find_element_by_xpath(self.dictionary_xpath)

    def get_product_user(self):
        return self.driver.find_element_by_xpath(self.product_user_xpath)

    def get_equipment_manage(self):
        return self.driver.find_element_by_xpath(self.equipment_manage_xpath)

    def get_product_kind_manage(self):
        return self.driver.find_element_by_xpath(self.product_kind_manage_xpath)

    def get_color_manage(self):
        return self.driver.find_element_by_xpath(self.color_manage_xpath)

    def do_click_team_group_manage(self):
        '''
        进入班组管理
        '''
        self.get_user_schedule().click()
        self.get_team_group_manage().click()
        sleep(1)

    def do_click_user_equipment(self):
        '''
        进入机台绑定
        '''
        self.get_user_schedule().click()
        self.get_user_equipment().click()
        sleep(1)

    def do_click_schedule_manage(self):
        '''
        进入排班管理
        '''
        self.get_user_schedule().click()
        self.get_schedule_manage().click()
        sleep(1)

    def do_click_dictionary(self):
        '''
        进入数据字典
        '''
        self.get_base_data().click()
        self.get_dictionary().click()
        sleep(1)

    def do_click_product_user(self):
        '''
        进入生产人员
        '''
        self.get_base_data().click()
        self.get_product_user().click()
        sleep(1)

    def do_click_equipment_manage(self):
        '''
        进入生产设备
        '''
        self.get_base_data().click()
        self.get_equipment_manage().click()
        sleep(1)

    def do_click_product_kind_manage(self):
        '''
        进入品种管理
        '''
        self.get_base_data().click()
        self.get_product_kind_manage().click()
        sleep(1)

    def do_click_color_manage(self):
        '''
        进入颜色管理
        '''
        self.get_base_data().click()
        self.get_color_manage().click()
        sleep(1)

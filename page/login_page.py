# -*- encoding: utf-8 -*-
"""
@File    : login_page.py
@Time    : 2021/6/3
@Author  : 陈晖
"""
from common.log import logger
from common.webdriver import WebDriver
from config import setting


class LoginPage:

    username_xpath = '//input[@placeholder="请输入公司码"]'
    password_xpath = '//input[@placeholder="请输入账号名或手机号"]'
    login_xpath = '//input[@placeholder="请输入密码"]'

    def __init__(self):
        self.driver = WebDriver.get_webdriver()

    def get_username(self):
        return self.driver.find_element_by_xpath(self.username_xpath)

    def get_password(self):
        return self.driver.find_element_by_xpath(self.password_xpath)

    def get_login_button(self):
        return self.driver.find_element_by_xpath(self.login_xpath)

    def do_login(self, username, password):
        logger.info('登录')
        self.get_username().send_keys(username)
        self.get_password().send_keys(password)
        self.get_login_button().click()


if __name__ == '__main__':
    WebDriver.init_webdriver()
    WebDriver.open(setting.mes_test_url)
    LoginPage().do_login('13764757307', '123456')
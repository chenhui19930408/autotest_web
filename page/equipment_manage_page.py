# -*- encoding: utf-8 -*-
"""
@File    : equipment_manage_page.py
@Time    : 2021/6/30
@Author  : 陈晖
"""
from time import sleep
from common.webdriver import WebDriver


class EquipmentManagePage:

    search_box_workshop_xpath = '//*[@id="app"]/div/div[2]/div[2]/div[2]/div/div[1]/form/div[1]/div/div/div/input'
    search_box_process_xpath = '//*[@id="app"]/div/div[2]/div[2]/div[2]/div/div[1]/form/div[2]/div/div/div[1]/input'
    search_box_type_xpath = '//*[@id="app"]/div/div[2]/div[2]/div[2]/div/div[1]/form/div[3]/div/div/div[1]/input'
    search_box_name_xpath = '//*[@id="app"]/div/div[2]/div[2]/div[2]/div/div[1]/form/div[4]/div/div/input'
    button_search_xpath = '//span[text()="搜索"]'
    button_reset_xpath = '//span[text()="重置条件"]'
    button_add_xpath = '//span[text()="新增"]'

    def __init__(self):
        self.driver = WebDriver.get_webdriver()

    def get_search_box_workshop(self):
        return self.driver.find_element_by_xpath(self.search_box_workshop_xpath)

    def get_search_box_process(self):
        return self.driver.find_element_by_xpath(self.search_box_process_xpath)

    def get_search_box_type(self):
        return self.driver.find_element_by_xpath(self.search_box_type_xpath)

    def get_search_box_name(self):
        return self.driver.find_element_by_xpath(self.search_box_name_xpath)

    def get_button_search(self):
        return self.driver.find_element_by_xpath(self.button_search_xpath)

    def get_button_reset(self):
        return self.driver.find_element_by_xpath(self.button_reset_xpath)

    def get_button_add(self):
        return self.driver.find_element_by_xpath(self.button_add_xpath)

    def do_select_workshop(self, workshop):
        self.get_search_box_workshop().click()
        self.driver.find_element_by_xpath('//div[@x-placement="bottom-start"]//span[text()="'+workshop+'"]').click()

    def do_clear_workshop(self):
        self.get_search_box_workshop().click()
        self.driver.find_element_by_xpath('//input[@placeholder="工厂"]/../span/span/em[2]').click()

    def do_select_process(self, process):
        self.get_search_box_process().click()
        self.driver.find_element_by_xpath('//div[@x-placement="bottom-start"]//span[text()="'+process+'"]').click()

    def do_clear_process(self):
        self.get_search_box_process().click()
        self.driver.find_element_by_xpath('//input[@placeholder="工序"]/../span/span/em[2]').click()

    def do_select_type(self, equipment_type):
        self.get_search_box_type().click()
        self.driver.find_element_by_xpath('//div[@x-placement="bottom-start"]//span[text()="'+equipment_type+'"]').click()

    def do_clear_type(self):
        self.get_search_box_type().click()
        self.driver.find_element_by_xpath('//input[@placeholder="设备类别"]/../span/span/em[2]').click()

    def do_search(self, workshop=None, process=None, type=None, name=None):
        if workshop:
            self.do_select_workshop(workshop)
        if process:
            self.do_select_process(process)
        if type:
            self.do_select_type(type)
        if name:
            self.get_search_box_name().clear()
            self.get_search_box_name().send_keys(name)
        self.get_button_search().click()
        sleep(1)

    def do_reset(self):
        self.get_button_reset().click()

    def do_add(self):
        self.get_button_add().click()

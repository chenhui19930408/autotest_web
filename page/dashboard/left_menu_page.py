# -*- encoding: utf-8 -*-
"""
@File    : left_menu_page.py
@Time    : 2021/7/23
@Author  : 陈晖
"""
from time import sleep

from common.webdriver import WebDriver


class LeftMenuPage:

    show_menu = '//*[@id="scaleBox"]/div/div/div/div[1]/div/div[1]/div[1]/div[1]'

    def __init__(self):
        self.driver = WebDriver.get_webdriver()

    def get_show_menu(self):
        return self.driver.find_element_by_xpath(self.show_menu)

    def do_click_show_menu(self):
        self.get_show_menu().click()

    def do_enter_board(self, first, second):
        self.do_click_show_menu()
        sleep(1)
        self.driver.find_element_by_xpath('//p[contains(text(),"'+first+'")]').click()
        self.driver.find_element_by_xpath('//div[contains(text(),"' + second + '")]').click()

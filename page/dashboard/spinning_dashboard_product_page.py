# -*- encoding: utf-8 -*-
"""
@File    : left_menu_page.py
@Time    : 2021/7/23
@Author  : 陈晖
"""
from common.webdriver import WebDriver


class SpinningDashboardProductPage:

    # 当日总产量
    today_output_total = '//span[contains(text(),"当日总产量")]/../span[1]'
    # 当日总能耗
    today_energy_total = '//span[contains(text(),"当日总能耗")]/../span[1]'
    # 员工总数
    emp_num = '//span[contains(text(),"员工总数")]/../span[1]'
    # 当前运行设备
    run_machine = '//span[contains(text(),"当前运行设备")]/../span[1]'
    # 当前停止设备
    stop_machine = '//span[contains(text(),"当前停止设备")]/../span[1]'
    # 开台品种
    product_run_num = '//span[contains(text(),"开台品种")]/../span[1]'
    # 最高锭速
    top_speed = '//span[contains(text(),"最高锭速")]/../span[1]'
    # 平均支数
    count_avg = '//span[contains(text(),"平均支数")]/../span[1]'
    # 昨日总产量
    yesterday_output_total = '//span[contains(text(),"昨日总产量")]/../span[1]'
    # 昨日总能耗
    yesterday_energy_total = '//span[contains(text(),"昨日总能耗")]/../span[1]'
    # 今日产量趋势（今日累计产量）
    today_output_trend_total_output = '//div[contains(text(),"今日累计产量")]/../div[2]'
    # 今日产量趋势（实际时间数据）
    today_output_trend_current_data = '//div[contains(text(),"今日产量趋势")]/../../../div[2]/div/div/div[2]/div/div[2]'
    # 今日产量趋势（实际产量数据）
    today_output_trend_current_output = '//div[contains(text(),"今日产量趋势")]/../../../div[2]/div/div/div[2]/div/div[2]/div'
    # 今日能耗趋势（今日总能耗）
    today_energy_trend_total_energy = '//div[contains(text(),"今日总能耗")]/../div[2]'
    # 今日能耗趋势（今日平均吨纱能耗）
    today_energy_trend_ton_energy = '//div[contains(text(),"今日平均吨纱能耗")]/../div[2]'
    # 今日能耗趋势（实际时间数据）
    today_energy_trend_current_data = '//div[contains(text(),"今日能耗趋势")]/../../../div[2]/div/div/div[2]/div/div[2]/div'
    # 今日能耗趋势（实际能耗数据）
    today_energy_trend_current_energy = '//div[contains(text(),"今日能耗趋势")]/../../../div[2]/div/div/div[2]/div/div[2]/div/div'
    # 实时机台状态（设备总数）
    status_total_machine_num = '//div[text()="实时机台状态"]/../../..//div[contains(text(),"设备总数")]'
    # 实时机台状态（正常运行）
    status_run_machine_num = '//div[text()="实时机台状态"]/../../..//*[name()="tspan" and contains(text(),"正常运行")]'
    # 实时机台状态（设备停机）
    status_stop_machine_num = '//div[text()="实时机台状态"]/../../..//*[name()="tspan" and contains(text(),"设备停机")]'
    # 实时机台状态（设备离线）
    status_offline_machine_num = '//div[text()="实时机台状态"]/../../..//*[name()="tspan" and contains(text(),"设备离线")]'
    # 实时机台状态（内网故障）
    status_failure_machine_num = '//div[text()="实时机台状态"]/../../..//*[name()="tspan" and contains(text(),"内网故障")]'
    # 今日产量达标率
    today_efficiency = '//div[text()="今日产量达标率"]/../../..//span'
    # 今日品种产量（品种）
    today_pro_output_product = ".//div[text()='今日品种产量']/../../../div[2]/div/div/div[2]"
    # 今日品种产量（产量）
    today_pro_output_output = ".//div[contains(text(),'今日品种产量')]/../../../div[2]/div/div/div[2]/div"
    # 最近班次产量（班组）
    last_group_output = ".//div[contains(text(),'最近班次产量')]/./../.././../div[2]/div/div/div[2]"

    def __init__(self):
        self.driver = WebDriver.get_webdriver()

    def get_today_output_total(self):
        return self.driver.find_element_by_xpath(self.today_output_total).text

    def get_today_energy_total(self):
        return self.driver.find_element_by_xpath(self.today_energy_total).text

    def get_emp_num(self):
        return self.driver.find_element_by_xpath(self.emp_num).text

    def get_run_machine(self):
        return self.driver.find_element_by_xpath(self.run_machine).text

    def get_stop_machine(self):
        return self.driver.find_element_by_xpath(self.stop_machine).text

    def get_product_run_num(self):
        return self.driver.find_element_by_xpath(self.product_run_num).text

    def get_top_speed(self):
        return self.driver.find_element_by_xpath(self.top_speed).text

    def get_count_avg(self):
        return self.driver.find_element_by_xpath(self.count_avg).text

    def get_yesterday_output_total(self):
        return self.driver.find_element_by_xpath(self.yesterday_output_total).text

    def get_yesterday_energy_total(self):
        return self.driver.find_element_by_xpath(self.yesterday_energy_total).text

    def get_trend_total_output(self):
        return self.driver.find_element_by_xpath(self.today_output_trend_total_output).text

    def get_today_output_trend_current_data(self):
        return self.driver.find_element_by_xpath(self.today_output_trend_current_data).text

    def get_today_output_trend_current_output(self):
        return self.driver.find_element_by_xpath(self.today_output_trend_current_output).text

    def get_trend_total_energy(self):
        return self.driver.find_element_by_xpath(self.today_energy_trend_total_energy).text

    def get_trend_ton_energy(self):
        return self.driver.find_element_by_xpath(self.today_energy_trend_ton_energy).text

    def get_today_energy_trend_current_data(self):
        return self.driver.find_element_by_xpath(self.today_energy_trend_current_data).text

    def get_today_energy_trend_current_energy(self):
        return self.driver.find_element_by_xpath(self.today_energy_trend_current_energy).text

    def get_today_efficiency(self):
        return self.driver.find_element_by_xpath(self.today_efficiency).text

    def get_total_machine_num(self):
        return self.driver.find_element_by_xpath(self.status_total_machine_num).text

    def get_run_machine_num(self):
        return self.driver.find_element_by_xpath(self.status_run_machine_num).text

    def get_stop_machine_num(self):
        return self.driver.find_element_by_xpath(self.status_stop_machine_num).text

    def get_offline_machine_num(self):
        return self.driver.find_element_by_xpath(self.status_offline_machine_num).text

    def get_failure_machine_num(self):
        return self.driver.find_element_by_xpath(self.status_failure_machine_num).text

    def get_today_pro_output_product(self):
        return self.driver.find_element_by_xpath(self.today_pro_output_product).text

    def get_today_pro_output_output(self):
        return self.driver.find_element_by_xpath(self.today_pro_output_output).text

    def get_last_group_output(self):
        return self.driver.find_element_by_xpath(self.last_group_output).text

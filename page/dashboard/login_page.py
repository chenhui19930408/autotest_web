# -*- encoding: utf-8 -*-
"""
@File    : login_page.py
@Time    : 2021/6/3
@Author  : 陈晖
"""
from time import sleep

from common.log import logger
from common.webdriver import WebDriver


class LoginPage:

    username_xpath = '//input[@type="text"]'
    password_xpath = '//input[@type="password"]'
    login_button_xpath = '//button[@type="button"]'
    weave_factory_xpath = '//div[text()="选择针织工厂:"]/../div[2]/div[1]'
    woven_factory_xpath = '//div[text()="选择梭织工厂:"]/../div[2]/div[1]'
    confirm_xpath = '//div[text()="确定"]'

    def __init__(self):
        self.driver = WebDriver.get_webdriver()

    def get_username(self):
        return self.driver.find_element_by_xpath(self.username_xpath)

    def get_password(self):
        return self.driver.find_element_by_xpath(self.password_xpath)

    def get_login_button(self):
        return self.driver.find_element_by_xpath(self.login_button_xpath)

    def get_weave_factory(self):
        return self.driver.find_element_by_xpath(self.weave_factory_xpath)

    def get_woven_factory(self):
        return self.driver.find_element_by_xpath(self.woven_factory_xpath)

    def get_confirm(self):
        return self.driver.find_element_by_xpath(self.confirm_xpath)

    def do_select_weave_factory(self, weave_factory):
        self.get_weave_factory().click()
        xpath = '//div[text()="选择针织工厂:"]/../div[2]/div[2]//div[contains(text(),"'+weave_factory+'")]'
        self.driver.find_element_by_xpath(xpath).click()

    def do_select_woven_factory(self, woven_factory):
        self.get_woven_factory().click()
        xpath = '//div[text()="选择梭织工厂:"]/../div[2]/div[2]//div[contains(text(),"'+woven_factory+'")]'
        self.driver.find_element_by_xpath(xpath).click()

    def do_login(self, username, password, weave_factory, woven_factory):
        logger.info('登录')
        self.get_username().send_keys(username)
        self.get_password().send_keys(password)
        self.get_login_button().click()
        sleep(1)
        self.do_select_weave_factory(weave_factory)
        self.do_select_woven_factory(woven_factory)
        self.get_confirm().click()
        sleep(1)

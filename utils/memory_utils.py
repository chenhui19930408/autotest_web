import psutil


# 获取所有chrome进程
def get_chrome_process():
    chrome_pids = []
    for pid in psutil.process_iter():
        if 'chrome' in pid.name():
            chrome_pids.append(pid)
    return chrome_pids


# 杀死指定进程
def kill_process(name='chrome'):
    for pid in psutil.process_iter():
        if name in pid.name():
            pid.kill()


# 获取电脑已使用的内存
def get_computer_used_memory():
    mem = psutil.virtual_memory()
    used_mem = float(mem.used) / 1024 / 1024
    return used_mem


# 获取指定程序的内存情况
def get_memory_by_process(name='chrome'):
    memory = 0
    for process in psutil.process_iter():
        if name in process.name():
            memory = memory + process.memory_info().vms / 1024 / 1024
    return memory


# 获取指定线程ID的内存情况
def get_memory_by_pid(pid):
    memory = 0
    for process in psutil.process_iter():
        if pid == process.pid:
            memory = memory + process.memory_info().vms / 1024 / 1024
    return memory


if __name__ == '__main__':
    print(kill_process())

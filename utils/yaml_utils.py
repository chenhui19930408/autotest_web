# -*- encoding: utf-8 -*-
"""
@File    : yaml_utils.py
@Time    : 2021/6/3
@Author  : 陈晖
"""
import yaml
from cataloguelist import DirPath


class YamlUtil:
    '''
    yaml文件读取的工具类，可以在里面定义对yaml文件操作的方法
    '''
    def __init__(self, yaml_file):
        self.yaml_file = yaml_file

    def read_yaml(self):
        with open(self.yaml_file, encoding="utf-8") as f:
            value = yaml.load(f, Loader=yaml.FullLoader)  # 文件流，加载方式
            return value


if __name__ == '__main__':
    print(YamlUtil().read_yaml())

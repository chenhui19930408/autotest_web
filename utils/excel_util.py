import xlrd
from cataloguelist import DirPath
from common.log import logger
from config import setting


class ExcelUtils:

    book = None
    sheet = None
    book_name = ''
    totle_row = 0
    totle_col = 0

    def __init__(self, filepath, sheet_name=None):
        filename = filepath.split('/')
        self.book_name = filename[len(filename) - 1]
        self.book = xlrd.open_workbook(filepath)
        if sheet_name:
            self.sheet = self.book.sheet_by_name(sheet_name)
        else:
            self.sheet = self.book.sheet_by_index(0)
        self.totle_row = self.sheet.nrows
        self.totle_col = self.sheet.ncols

    def readByCell(self, row, col):
        logger.info('read excel(' + str(self.book_name) + ').sheet(' + str(self.sheet.name) + ').cell(' + str(row) + ',' + str(col) + ')')
        return self.sheet.cell(row, col).value

    def getTestData(self):
        list = self.readData()
        param_name = ','.join(list[0])
        data = list[1:]
        return param_name, data

    def readData(self):
        list = []
        list.append([])
        url_col = 0
        for col in range(self.totle_col):
            if self.sheet.cell(0, col).value == 'post_url':
                url_col = col
            list[0].append(self.sheet.cell(0, col).value)
        for row in range(self.totle_row - 1):
            list.append([])
            row = row + 1
            for col1 in range(self.totle_col):
                if col1 == url_col:
                    value = setting.dashboard_host_api + self.sheet.cell(row, col1).value
                else:
                    value = self.sheet.cell(row, col1).value
                list[row].append(value)
        return list


if __name__ == "__main__":
    url = ExcelUtils(DirPath.testcase_path + '/schedule_manage/data/change_daily_schedule.xls').getTestData()
    print(url)

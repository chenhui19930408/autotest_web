import csv
import os
import time

from cataloguelist import DirPath
from utils.memory_utils import *

file_path = DirPath.memory_test_report_path
file_name = time.strftime("%Y-%m-%d_%H-%M-%S") + '.csv'
report_name = file_path + '/' + file_name


def create_csv_file():
    if not os.path.exists(file_path):
        os.mkdir(file_path)
    with open(report_name, 'a', newline='') as f:
        ff = csv.writer(f)
        ff.writerow(['Time', 'Action', 'vms'])


def write_data_to_csv(action=''):
    now = time.strftime("%Y-%m-%d %H:%M:%S")
    vms = get_memory_by_process('chrome')
    content_list = [now] + [action] + [vms]
    print(content_list)
    with open(report_name, 'a', newline='')as f:
        ff = csv.writer(f)
        ff.writerow(content_list)


if __name__ == '__main__':
    for i in range(10000):
        time.sleep(1)
        write_data_to_csv('test')

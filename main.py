import os
import pytest
from cataloguelist import DirPath


if __name__ == '__main__':
    pytest.main(['-s', DirPath.testcase_path, '--alluredir', DirPath.report_source])
    os.system('{} generate {} -o {} --clean'.format(DirPath.allure, DirPath.report_source, DirPath.report_html))

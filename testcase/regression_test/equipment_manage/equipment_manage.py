import allure
import pytest

from cataloguelist import DirPath
from common.assert_utils import AssertUtils
from common.common_fun import CommonFun
from common.webdriver import WebDriver
from utils.yaml_utils import YamlUtil
from config import setting
from page.equipment_manage_page import EquipmentManagePage
from page.left_menu_page import LeftMenuPage


class TestEquipmentManage:

    def setup_class(self):
        CommonFun().open_client_and_login(setting.login_username, setting.login_password)
        LeftMenuPage().do_click_equipment_manage()

    def teardown_class(self):
        WebDriver.close()

    @allure.story('搜索工厂')
    @pytest.mark.parametrize('args', YamlUtil(
        DirPath.testcase_path+'/equipment_manage/data/test_01_search_workshop.yaml').read_yaml())
    def test_01_search_workshop(self, args):
        EquipmentManagePage().do_search(workshop=args.get('workshop'))
        assert AssertUtils().assert_element_len('//*[text()="二分厂"]', 1)
        EquipmentManagePage().do_clear_workshop()

    @allure.story('搜索工序')
    @pytest.mark.parametrize('args', YamlUtil(
        DirPath.testcase_path+'/equipment_manage/data/test_02_search_process.yaml').read_yaml())
    def test_02_search_process(self, args):
        EquipmentManagePage().do_search(process=args.get('process'))
        assert AssertUtils().assert_element_len('//*[text()="细纱"]', 1)
        EquipmentManagePage().do_clear_process()

    @allure.story('搜索设备类别')
    @pytest.mark.parametrize('args', YamlUtil(
        DirPath.testcase_path+'/equipment_manage/data/test_03_search_type.yaml').read_yaml())
    def test_03_search_type(self, args):
        EquipmentManagePage().do_search(type=args.get('type'))
        assert AssertUtils().assert_element_len('//*[text()="检测设备"]', 1)
        EquipmentManagePage().do_clear_type()

    @allure.story('搜索设备名称')
    @pytest.mark.parametrize('args', YamlUtil(
        DirPath.testcase_path+'/equipment_manage/data/test_04_search_name.yaml').read_yaml())
    def test_04_search_name(self, args):
        EquipmentManagePage().do_search(name=args.get('name'))
        assert AssertUtils().assert_element_len('//table//tbody/tr', 1)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/equipment_manage/test_equipment_manage.py'])

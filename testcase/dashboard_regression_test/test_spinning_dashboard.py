from datetime import datetime, timedelta
from time import sleep

import allure
import pytest

from cataloguelist import DirPath
from common.Request import Request
from common.assert_utils import AssertUtils
from common.webdriver import WebDriver
from config import setting
from page.dashboard.left_menu_page import LeftMenuPage
from page.dashboard.login_page import LoginPage
from page.dashboard.spinning_dashboard_product_page import SpinningDashboardProductPage
from utils.excel_util import ExcelUtils


class TestSpinningDashboard:

    def setup_class(self):
        WebDriver.init_webdriver()
        WebDriver.open(setting.dashboard_test_url)
        LoginPage().do_login(setting.dashboard_login_username, setting.dashboard_login_password,
                             setting.dashboard_login_key1, setting.dashboard_login_key2)
        LeftMenuPage().do_enter_board('纱线', '生产总览-纱线')
        sleep(5)

    def teardown_class(self):
        WebDriver.close()

    def get_header(self):
        header = {
            "auth-token": "48bc639ed7fa44348b397e79b02ea317",
            "corp-code": "9001",
            "orgcode": "9001"
        }
        return header

    @allure.story('当日总产量')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_01.xls').getTestData())
    def test_01(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_today_output_total()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('当日总能耗')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_02.xls').getTestData())
    def test_02(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_today_energy_total()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('员工总数')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_03.xls').getTestData())
    def test_03(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_emp_num()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('当前运行设备')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_04.xls').getTestData())
    def test_04(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_run_machine()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('当前停止设备')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_05.xls').getTestData())
    def test_05(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_stop_machine()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('开台品种')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_06.xls').getTestData())
    def test_06(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_product_run_num()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('最高锭速')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_07.xls').getTestData())
    def test_07(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_top_speed()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('平均支数')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_08.xls').getTestData())
    def test_08(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_count_avg()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('昨日总产量')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_09.xls').getTestData())
    def test_09(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_yesterday_output_total()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('昨日总能耗')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_10.xls').getTestData())
    def test_10(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_yesterday_energy_total()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('今日产量趋势（今日累计产量）')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_11.xls').getTestData())
    def test_11(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_trend_total_output()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('今日能耗趋势（今日平均吨纱能耗）')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_12.xls').getTestData())
    def test_12(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_trend_ton_energy()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('今日能耗趋势（今日总能耗）')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_13.xls').getTestData())
    def test_13(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_trend_total_energy()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('实时机台状态（设备总数）')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_14.xls').getTestData())
    def test_14(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_total_machine_num()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('实时机台状态（正常运行）')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_15.xls').getTestData())
    def test_15(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_run_machine_num()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('实时机台状态（设备停机）')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_16.xls').getTestData())
    def test_16(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_stop_machine_num()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('实时机台状态（设备离线）')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_17.xls').getTestData())
    def test_17(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_offline_machine_num()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('实时机台状态（内网故障）')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_18.xls').getTestData())
    def test_18(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_failure_machine_num()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('今日产量达标率')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_19.xls').getTestData())
    def test_19(self, post_url, post_data, response):
        actual_value = SpinningDashboardProductPage().get_today_efficiency()
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        assert AssertUtils().assert_json_path(res, response, actual_value)

    @allure.story('今日产量趋势（实际时间+实时产量）')
    @pytest.mark.parametrize(
        *ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_20.xls').getTestData())
    def test_20(self, post_url, post_data, assert_key1, assert_key2):
        WebDriver.refresh()
        sleep(5)
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        for i in range(5):
            sleep(2)
            actual_data_value = SpinningDashboardProductPage().get_today_output_trend_current_data()
            list = [assert_key1,assert_key2]
            assert AssertUtils().assert_dashboard_trend(res, list, actual_data_value)

    @allure.story('今日能耗趋势（实际时间+实时能耗）')
    @pytest.mark.parametrize(
        *ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_21.xls').getTestData())
    def test_21(self, post_url, post_data, assert_key1, assert_key2):
        WebDriver.refresh()
        sleep(5)
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        for i in range(5):
            sleep(2)
            actual_data_value = SpinningDashboardProductPage().get_today_energy_trend_current_data()
            list = [assert_key1, assert_key2]
            assert AssertUtils().assert_dashboard_trend(res, list, actual_data_value)

    @allure.story('今日品种产量（实际品种+品种产量）')
    @pytest.mark.parametrize(
        *ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_22.xls').getTestData())
    def test_22(self, post_url, post_data, assert_key1, assert_key2):
        WebDriver.refresh()
        sleep(5)
        post_data = post_data.replace("$data", datetime.now().strftime('%Y-%m-%d'))
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        for i in range(5):
            sleep(2)
            actual_product_value = SpinningDashboardProductPage().get_today_pro_output_product()
            list = [assert_key1, assert_key2]
            assert AssertUtils().assert_dashboard_trend(res, list, actual_product_value)

    @allure.story('最近班次产量（实际品种+品种产量）')
    @pytest.mark.parametrize(
        *ExcelUtils(DirPath.testcase_path + '/dashboard_regression_test/data/test_23.xls').getTestData())
    def test_23(self, post_url, post_data, assert_key1, assert_key2):
        WebDriver.refresh()
        sleep(5)
        post_data = post_data.replace("$dateTo", datetime.now().strftime('%Y-%m-%d')).\
            replace("$dateFrom", (datetime.now() + timedelta(-7)).strftime('%Y-%m-%d'))
        res = Request().postJson(url=post_url, json=eval(post_data), header=self.get_header())
        for i in range(5):
            sleep(2)
            actual_value = SpinningDashboardProductPage().get_last_group_output()
            list = [assert_key1, assert_key2]
            assert AssertUtils().assert_dashboard_trend(res, list, actual_value)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/dashboard_regression_test/test_spinning_dashboard.py'])

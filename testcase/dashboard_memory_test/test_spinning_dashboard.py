import time

import allure
import pytest

from cataloguelist import DirPath
from common.webdriver import WebDriver
from config import setting
from page.dashboard.left_menu_page import LeftMenuPage
from page.dashboard.login_page import LoginPage
from utils.csv_utils import write_data_to_csv
from utils.memory_utils import kill_process


class TestSpinningDashboard:

    def setup_method(self):
        kill_process()
        WebDriver.init_webdriver()
        WebDriver.open(setting.dashboard_test_url)
        write_data_to_csv('打开浏览器')

    def teardown_method(self):
        WebDriver.close()

    @allure.story('纱线大屏内存测试')
    def test_01(self):
        LoginPage().do_login(setting.dashboard_login_username, setting.dashboard_login_password,
                             setting.dashboard_login_key1, setting.dashboard_login_key2)
        LeftMenuPage().do_enter_board('纱线', '生产总览-纱线')
        for i in range(10800):
            time.sleep(1)
            write_data_to_csv('循环记录第' + str(i) + '秒')


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/dashboard_memory_test/test_spinning_dashboard.py'])

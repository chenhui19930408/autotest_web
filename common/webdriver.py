# -*- encoding: utf-8 -*-
"""
@File    : webdriver.py
@Time    : 2021/6/3
@Author  : 陈晖
"""
from time import sleep

from selenium import webdriver


class WebDriver:
    '''
    封装webdriver
    webdriver用单例模式，实例化一次后面直接调用类方法，直接操作wendriver就行
    '''

    __driver = None

    def __init__(self):
        pass

    @classmethod
    def init_webdriver(cls):
        if cls.__driver is None:
            cls.__driver = webdriver.Chrome()
            cls.__driver.maximize_window()
            cls.__driver.implicitly_wait(5)

    @classmethod
    def get_webdriver(cls):
        return cls.__driver

    @classmethod
    def close(cls):
        cls.__driver.close()
        cls.__driver = None

    @classmethod
    def open(cls, url):
        cls.__driver.get(url)

    @classmethod
    def refresh(cls):
        if cls.__driver:
            cls.__driver.refresh()
            sleep(2)


import allure
import requests

from common.log import logger


class Request:

    session = requests.session()

    def __init__(self):
        pass

    @allure.step('发送data请求')
    def postData(self, url, data=None, header=None):
        r = self.session.post(url=url, data=data, headers=header)
        logger.info('post: ' + url + '出参: ' + str(data) + ',返回: ' + r.text + '\n')
        return r

    @allure.step('发送json请求')
    def postJson(self, url, json=None, header=None):
        r = self.session.post(url=url, json=json, headers=header)
        logger.info('post: ' + url + '出参: ' + str(json) + ',返回: ' + r.text + '\n')
        return r

    @allure.step('发送get请求')
    def get(self, url, params=None, header=None):
        r = self.session.get(url=url, params=params, headers=header)
        logger.info('get_url: ' + url + ' ,response: ' + r.text + '\n')
        return r


if __name__ == '__main__':
    pass

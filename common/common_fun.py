from common.webdriver import WebDriver
from config import setting
from page.login_page import LoginPage


class CommonFun:

    def __init__(self):
        self.driver = WebDriver.get_webdriver()

    def open_client_and_login(self, username, password):
        WebDriver.init_webdriver()
        WebDriver.open(setting.test_url)
        self.login(username, password)

    def login(self, username, password):
        LoginPage().do_login(username, password)

    def get_value_by_xpath(self, xpath):
        return self.driver.find_element_by_xpath(xpath).text


if __name__ == '__main__':
    CommonFun().open_client_and_login(setting.login_username, setting.login_password)
# -*- encoding: utf-8 -*-
"""
@File    : assertions.py
@Time    : 2021/6/3
@Author  : 陈晖
"""
import jsonpath

from common.webdriver import WebDriver


class AssertUtils:
    '''
    这里做断言，实例化断言类以后，直接调用断言方法就行了
    '''
    def __init__(self):
        self.driver = WebDriver.get_webdriver()

    def assert_element_exit(self, xpath):
        '''
        通过指定的xpath路径在页面上定位元素，有就返回true没有返回false
        :param xpath: 指定xpath路径
        :return:
        '''
        try:
            self.driver.find_element_by_xpath(xpath)
            return True
        except:
            return False

    def assert_element_len(self, xpath, length):
        '''
        通过指定的xpath的路径在页面上定位元素的个数，和期望个数相同返回true，不同返回false
        :param xpath: 指定的xpath路径
        :param length: 期望个数
        :return:
        '''
        try:
            elements = self.driver.find_elements_by_xpath(xpath)
            if len(elements) == length:
                return True
            else:
                return False
        except:
            return False

    def assert_json_path(self, res, expect_jsonpath, actual_value):
        '''
        在接口返回值res中，断言指定jsonpath下的值和实际值是否相同，相同返回true，不同返回false
        :param res: 接口返回的json格式数据
        :param expect_jsonpath: 通过jsonpath定位到期望值
        :param actual_value: 页面获取的实际值
        :return:
        '''
        if res.json()['status'] == 200:
            expect_value = jsonpath.jsonpath(res.json(), expect_jsonpath)[0]
            if str(expect_value) in actual_value:
                return True
            else:
                return False
        else:
            return False

    def assert_dashboard_trend(self, res, expect_key_list, actual_value):
        '''
        针对大屏动态趋势图的断言方法
        :param res:调用接口返回的数据
        :param expect_key_list:需要断言的jsonpath
        :param actual_value:页面上捕捉到的数据
        :return:根据断言的jsonpath再res数据中找到对应的值，如果这些值同时存在再actual_value中则判断正确，否则返回错误
        '''
        result = False
        if res.json()['status'] == 200:
            res_list = res.json()['res']
            for i in range(len(res_list)):
                equal_flag = True
                for expect_key in expect_key_list:
                    expect_value = jsonpath.jsonpath(res_list[i], expect_key)[0]
                    if not str(expect_value) in actual_value:
                        equal_flag = False
                if equal_flag:
                    result = True
                    break
        return result

import configparser

from cataloguelist import DirPath


# 读取配置文件
cf = configparser.ConfigParser()
cf.read(DirPath.config_path + "/config.ini", encoding='utf-8')
# 读取配置文件中的变量
mes_test_url = cf.get("mes", "test_url")
mes_login_username = cf.get("mes", "login_username")
mes_login_password = cf.get("mes", "login_password")
# 读取大屏测试的配置文件
if cf.get("dashboard", "evn") == 'test':
    dashboard_test_url = cf.get("dashboard", "test_dashboard_url")
    dashboard_login_username = cf.get("dashboard", "test_dashboard_login_username")
    dashboard_login_password = cf.get("dashboard", "test_dashboard_login_password")
    dashboard_login_key1 = cf.get("dashboard", "test_dashboard_login_key1")
    dashboard_login_key2 = cf.get("dashboard", "test_dashboard_login_key2")
    dashboard_host_api = cf.get("dashboard", "test_dashboard_host_api")
elif cf.get("dashboard", "evn") == 'online':
    dashboard_test_url = cf.get("dashboard", "online_dashboard_url")
    dashboard_login_username = cf.get("dashboard", "online_dashboard_login_username")
    dashboard_login_password = cf.get("dashboard", "online_dashboard_login_password")
    dashboard_login_key1 = cf.get("dashboard", "online_dashboard_login_key1")
    dashboard_login_key2 = cf.get("dashboard", "online_dashboard_login_key2")
    dashboard_host_api = cf.get("dashboard", "online_dashboard_host_api")
